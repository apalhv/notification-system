# Notification System

## Assumptions:
1. There exists tables which contain the data of different companies and their employees.
2. `company_id` and `last_modified_by` are obtained from the access token present in the Authorization header.
3. We can use join queries to reduce the number of DB calls here. But for now, I've created separate APIs for everything so that we can easily convert it to microservices in future and make it pluggable.

## Email Template
### Schema:
| **Field**        | **Type**      | **Description**      |
| ---------------- | ------------- | -------------------- |
| id               | UUID          | Primary Key          |
| company_id       | UUID          | Foreign Key          |
| name             | text          | Template name        |
| email_subject    | text          | Email Subject        |
| email_body       | text          | Email body in HTML   |
| last_modified_by | UUID          | User ID, Foreign Key |
| created_at       | timestamp     | Time of creation     |
| updated_at       | timestamp     | Latest Time of updation |

### APIs:
1. *Get an Email Template*  
   **Request:**  
   Method: `GET`  
   Endpoint: `/templates/{id}`  
   
   **Response Body:**
   ```json
    {
        "id": "23b94f39-c281-4807-8036-91e807eff21e",
        "company_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
        "name": "Greetings Template",
        "email_subject": "Welcome {{ employee_name }} to {{ company_name }}",
        "email_body": "<html>content</html>",
        "last_modified_by": "b8fe7020-fce1-11eb-9a03-0242ac130003",
        "created_at": "2021-08-14T09:24:20.754Z",
        "updated_at": "2021-08-14T09:24:20.754Z"
    }
   ```

2. *List all Email Templates*  
    **Request**  
    Method: `GET`  
    Endpoint: `/templates`  

    **Response Body:**
    ```json
    [
        {
            "id": "23b94f39-c281-4807-8036-91e807eff21e",
            "company_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
            "name": "Greetings Template",
            "email_subject": "Welcome {{ employee_name }} to {{ company_name }}",
            "email_body": "<html>content</html>",
            "last_modified_by": "b8fe7020-fce1-11eb-9a03-0242ac130003",
            "created_at": "2021-08-14T09:24:20.754Z",
            "updated_at": "2021-08-14T09:24:20.754Z"
        },
        ...
    ]
    ```

3. *Create a Template*  
    **Request**  
    Method: `POST`  
    Endpoint: `/templates`  

    **Request Body:**
    ```json
    {
        "name": "Greetings Template",
        "email_subject": "Welcome {{ employee_name }} to {{ company_name }}",
        "email_body": "<html>content</html>",
        "created_at": "2021-08-14T09:24:20.754Z",
        "updated_at": "2021-08-14T09:24:20.754Z"
    }
    ```

    **Response Body:**
   ```json
    {
        "id": "23b94f39-c281-4807-8036-91e807eff21e",
        "company_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
        "name": "Greetings Template",
        "email_subject": "Welcome {{ employee_name }} to {{ company_name }}",
        "email_body": "<html>content</html>",
        "last_modified_by": "b8fe7020-fce1-11eb-9a03-0242ac130003",
        "created_at": "2021-08-14T09:24:20.754Z",
        "updated_at": "2021-08-14T09:24:20.754Z"
    }
   ```

4. *Update a Template*  
    **Request**  
    Method: `PUT`  
    Endpoint: `/templates/{id}`  

    **Request Body:**
    ```json
    {
        "name": "Greetings Template",
        "email_subject": "Welcome {{ employee_name }} to {{ company_name }}",
        "email_body": "<html>content</html>",
        "created_at": "2021-08-14T09:24:20.754Z",
        "updated_at": "2021-08-14T09:24:20.754Z"
    }
    ```

    **Response Body:**
   ```json
    {
        "id": "23b94f39-c281-4807-8036-91e807eff21e",
        "company_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
        "name": "Greetings Template",
        "email_subject": "Welcome {{ employee_name }} to {{ company_name }}",
        "email_body": "<html>content</html>",
        "last_modified_by": "b8fe7020-fce1-11eb-9a03-0242ac130003",
        "created_at": "2021-08-14T09:24:20.754Z",
        "updated_at": "2021-08-14T09:24:20.754Z"
    }
   ```

5. *Delete a Template*  
    **Request**  
    Method: `DELETE`  
    Endpoint: `/templates/{id}`  

---

## Events
### Schema:
| **Field**        | **Type**      | **Description** |
| ---------------- | ------------- | --------------- |
| id               | UUID          | Primary Key     |
| company_id       | UUID          | Foriegn Key     |
| name             | text          | Event Name      |
| template_id      | text          | Foreign Key     |
| user_roles       | text[]        | Roles to be sent the notification|
| required_fields  | text[]        | Required fields for email template and Validation |
| last_modified_by | UUID          | User ID, Foreign Key |
| created_at       | timestamp     | Time of creation |
| updated_at       | timestamp     | Latest Time of updation |

### APIs:
1. *Get an Event*  
    **Request:**  
    Method: `GET`  
    Endpoint: `/events/{id}`  
    
    **Response Body:**
    ```json
    {
        "id": "23b94f39-c281-4807-8036-91e807eff21e",
        "company_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
        "name": "Add User",
        "template_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
        "user_roles": ['EMPLOYEE', 'ADMIN'],
        "required_fields": ['employee_id', 'esop_value'],
        "last_modified_by": "b8fe7020-fce1-11eb-9a03-0242ac130003",
        "created_at": "2021-08-14T09:24:20.754Z"
    }
    ```

2. *List all Events*  
    **Request:**  
    Method: `GET`  
    Endpoint: `/events/`  
    
    **Response Body:**
    ```json
    [
        {
            "id": "23b94f39-c281-4807-8036-91e807eff21e",
            "company_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
            "name": "Add User",
            "template_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
            "user_roles": ['EMPLOYEE', 'ADMIN'],
            "required_fields": ['employee_id', 'esop_value'],
            "last_modified_by": "b8fe7020-fce1-11eb-9a03-0242ac130003",
            "created_at": "2021-08-14T09:24:20.754Z"
        },
        ...
    ]
    ```
3. *Create an Event*  
    **Request:**  
    Method: `POST`  
    Endpoint: `/events/` 

    **Request Body:**  
    ```json
    {
        "name": "Add User",
        "template_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
        "user_roles": ['EMPLOYEE', 'ADMIN'],
        "required_fields": ['employee_id', 'esop_value'],
    }
     ```
    
    **Response Body:**
    ```json
    {
        "id": "23b94f39-c281-4807-8036-91e807eff21e",
        "company_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
        "name": "Add User",
        "template_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
        "user_roles": ['EMPLOYEE', 'ADMIN'],
        "required_fields": ['employee_id', 'esop_value'],
        "last_modified_by": "b8fe7020-fce1-11eb-9a03-0242ac130003",
        "created_at": "2021-08-14T09:24:20.754Z"
    }
    ```

4. *Update an Event*  
    **Request:**  
    Method: `PUT`  
    Endpoint: `/events/{id}` 

    **Request Body:**  
    ```json
    {
        "name": "Add User",
        "template_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
        "user_roles": ['EMPLOYEE', 'ADMIN'],
        "required_fields": ['employee_id', 'esop_value'],
    }
     ```
    
    **Response Body:**
    ```json
    {
        "id": "23b94f39-c281-4807-8036-91e807eff21e",
        "company_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
        "name": "Add User",
        "template_id": "4c6136b5-4462-4423-b9eb-b4be6783c850",
        "user_roles": ['EMPLOYEE', 'ADMIN'],
        "required_fields": ['employee_id', 'esop_value'],
        "last_modified_by": "b8fe7020-fce1-11eb-9a03-0242ac130003",
        "created_at": "2021-08-14T09:24:20.754Z"
    }
    ```
5. *Delete an Event*  
    **Request**  
    Method: `DELETE`  
    Endpoint: `/events/{id}`  

6. *Trigger an Event*  
    **Request**
    Method: `POST`  
    Endpoint: `/events/send`

    **Request Body**
    ```json
    {
        "event_id": "23b94f39-c281-4807-8036-91e807eff21e",
        "event_data": {
            "employee_id": "b8fe7020-fce1-11eb-9a03-0242ac130003",
            ...
        },
        "should_notify_users": true,
    }
    ```
    > If we provide `employee_id`, set `should_notify_users` to true and the field `user_roles` contains the role `EMPLOYEE` for that particular event, then only it sends the notification to that particular employee.

    > If not all the `event_data` is passed as mentioned in `required_fields` it throws appropriate errors.

---

## User/Roles Management

> **Assumption:** There already exists a table where the data of all the users is stored and there is an API which returns email id of all the users if we provide `company_id`, `employee_id` and `roles`.
